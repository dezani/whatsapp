app.controller('UsuarioListaCtrl',
     function($scope, UsuarioService, $state) { // ~ class

    $scope.usuarios = UsuarioService.readAll();

    $scope.newUser = function() {
        $state.go('usuario-create');
    }
}); 

app.controller('UsuarioCreateCtrl',
    function($scope, UsuarioService, $ionicHistory){

    $scope.usuario = {
        id: null,
        nome: 'Robin',
        email: 'robin@batcaverna.com',
        senha: '123'
    }

    $scope.salvar = function(usuario) {
        
        UsuarioService.create(usuario);
        $ionicHistory.goBack(-1);
    }
});

app.controller('RegistroCtrl', 
    function($scope, $ionicAuth, $ionicPopup, $state){

    $scope.usuario = {};

    $scope.salvar = function(usuario) {
        $ionicAuth.signup(usuario)
            .then(function() {
                $state.go('login');
            })
            .catch(function(error) {
                if(error.details[0] == "required_email")
                    $ionicPopup.alert({
                        title: "Falha no registro",
                        template: "Por favor, preencha seu e-mail."
                    });
            });
    }
});

        
app.controller('LoginCtrl', function($scope, $ionicAuth, $state, $ionicUser) {

    if ($ionicAuth.isAuthenticated()) {
        $state.go('tabs.usuarios');
    }

    $scope.usuario = {};

    $scope.entrar = function(usuario) {
        $ionicAuth.login('basic', usuario)
            .then(function() {
                var user = $ionicUser;
                $state.go('tabs.usuarios');
            });
    }

});

app.controller('ConfigCtrl', function($scope, $state, $ionicUser, $ionicAuth, $ionicPopup) {

    $scope.usuario = $ionicUser.details;

    $scope.salvar = function(usuario) {

        $ionicUser.details = usuario;
        $ionicUser.save()
            .then(function() {
                $ionicPopup.alert({
                    title: 'Sucesso',
                    template: 'Alteração realizada com sucesso.'
                    // okText: 'Entendeu?',
                    // okType: 'button-assertive'
                });

            })
            .catch(function(error) {

            });

    }

    $scope.logout = function() {
        $ionicAuth.logout();

        $state.go('login');
    }

});